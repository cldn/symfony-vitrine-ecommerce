<?php

namespace mi13\VitrineBundle\Entity;

/**
 * Article
 */
class Article
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $stock;

    /**
     * @var string
     */
    private $price;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Article
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Article
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return Article
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Article
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lignes;

    /**
     * @var \mi13\VitrineBundle\Entity\Categorie
     */
    private $categorie;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lignes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ligne
     *
     * @param \mi13\VitrineBundle\Entity\LigneCommande $ligne
     *
     * @return Article
     */
    public function addLigne(\mi13\VitrineBundle\Entity\LigneCommande $ligne)
    {
        $this->lignes[] = $ligne;

        return $this;
    }

    /**
     * Remove ligne
     *
     * @param \mi13\VitrineBundle\Entity\LigneCommande $ligne
     */
    public function removeLigne(\mi13\VitrineBundle\Entity\LigneCommande $ligne)
    {
        $this->lignes->removeElement($ligne);
    }

    /**
     * Get lignes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignes()
    {
        return $this->lignes;
    }

    /**
     * Set categorie
     *
     * @param \mi13\VitrineBundle\Entity\Categorie $categorie
     *
     * @return Article
     */
    public function setCategorie(\mi13\VitrineBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \mi13\VitrineBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lignes_commande;


    /**
     * Add lignesCommande
     *
     * @param \mi13\VitrineBundle\Entity\LigneCommande $lignesCommande
     *
     * @return Article
     */
    public function addLignesCommande(\mi13\VitrineBundle\Entity\LigneCommande $lignesCommande)
    {
        $this->lignes_commande[] = $lignesCommande;

        return $this;
    }

    /**
     * Remove lignesCommande
     *
     * @param \mi13\VitrineBundle\Entity\LigneCommande $lignesCommande
     */
    public function removeLignesCommande(\mi13\VitrineBundle\Entity\LigneCommande $lignesCommande)
    {
        $this->lignes_commande->removeElement($lignesCommande);
    }

    /**
     * Get lignesCommande
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignesCommande()
    {
        return $this->lignes_commande;
    }
}
