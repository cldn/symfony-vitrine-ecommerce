<?php

namespace mi13\VitrineBundle\Entity;

/**
 * LigneCommande
 */
class LigneCommande
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $quantite;

    /**
     * @var string
     */
    private $prix;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return LigneCommande
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set prix
     *
     * @param string $prix
     *
     * @return LigneCommande
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return string
     */
    public function getPrix()
    {
        return $this->prix;
    }
    /**
     * @var \mi13\VitrineBundle\Entity\Article
     */
    private $article;

    /**
     * @var \mi13\VitrineBundle\Entity\Commande
     */
    private $commande;


    /**
     * Set article
     *
     * @param \mi13\VitrineBundle\Entity\Article $article
     *
     * @return LigneCommande
     */
    public function setArticle(\mi13\VitrineBundle\Entity\Article $article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \mi13\VitrineBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set commande
     *
     * @param \mi13\VitrineBundle\Entity\Commande $commande
     *
     * @return LigneCommande
     */
    public function setCommande(\mi13\VitrineBundle\Entity\Commande $commande)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande
     *
     * @return \mi13\VitrineBundle\Entity\Commande
     */
    public function getCommande()
    {
        return $this->commande;
    }
}
