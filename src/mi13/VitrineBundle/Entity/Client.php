<?php

namespace mi13\VitrineBundle\Entity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Client
 */
class Client implements UserInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function getUsername() {
        return $this->getEmail();
    }
    
    public function getSalt() {
        return null;
    }
    
    public function isAdministrateur() {
        return ($this->getEmail() == 'admin@admin.fr');
    }
    
    public function getRoles() {
        if ($this->isAdministrateur()) {
            return array('ROLE_ADMIN');
        } else {
            return array('ROLE_USER');
        }
    }
    
    public function eraseCredentials() { }
    
    public function serialize() {
        return serialize(array($this-id, $this->name, $this-email, $this->password));
    }
    
    public function unserialize($serialized) {
        list($this->id, $this->name, $this->email, $this->password) = unserialize($serialized);
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Client
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $commandes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->commandes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add commande
     *
     * @param \mi13\VitrineBundle\Entity\Commande $commande
     *
     * @return Client
     */
    public function addCommande(\mi13\VitrineBundle\Entity\Commande $commande)
    {
        $this->commandes[] = $commande;

        return $this;
    }

    /**
     * Remove commande
     *
     * @param \mi13\VitrineBundle\Entity\Commande $commande
     */
    public function removeCommande(\mi13\VitrineBundle\Entity\Commande $commande)
    {
        $this->commandes->removeElement($commande);
    }

    /**
     * Get commandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommandes()
    {
        return $this->commandes;
    }
    
    public function __toString() {
        return $this->getEmail();
    }
}
