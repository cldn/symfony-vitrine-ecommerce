<?php

namespace mi13\VitrineBundle\Entity;

/**
 * Description of Panier
 *
 * @author denailly
 */
class Panier {
    private $contenu;
    
    public function __construct() {
        $this->contenu = array();
    }
    
    public function getContenu() {
        return $this->contenu;
    }
    
    public function ajoutArticle($articleId, $qte = 1) {
        if (isset($this->contenu[$articleId])) {
            // If the content already has this article, sum its existing quantity with the wanted quantity
            $this->contenu[$articleId] += $qte;
        } else {
            // Add the article and its quantity to the content
            $this->contenu[$articleId] = $qte;
        }
    }
    
    public function supprimeArticle($articleId) {
        unset($this->contenu[$articleId]);
    }
    
    public function viderPanier() {
        $this->contenu = array();
    }
}
