<?php

namespace mi13\VitrineBundle\Entity;

/**
 * Commande
 */
class Commande
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var bool
     */
    private $validated;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Commande
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return Commande
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return bool
     */
    public function getValidated()
    {
        return $this->validated;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lignes;

    /**
     * @var \mi13\VitrineBundle\Entity\Client
     */
    private $client;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lignes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ligne
     *
     * @param \mi13\VitrineBundle\Entity\LigneCommande $ligne
     *
     * @return Commande
     */
    public function addLigne(\mi13\VitrineBundle\Entity\LigneCommande $ligne)
    {
        $this->lignes[] = $ligne;

        return $this;
    }

    /**
     * Remove ligne
     *
     * @param \mi13\VitrineBundle\Entity\LigneCommande $ligne
     */
    public function removeLigne(\mi13\VitrineBundle\Entity\LigneCommande $ligne)
    {
        $this->lignes->removeElement($ligne);
    }

    /**
     * Get lignes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignes()
    {
        return $this->lignes;
    }

    /**
     * Set client
     *
     * @param \mi13\VitrineBundle\Entity\Client $client
     *
     * @return Commande
     */
    public function setClient(\mi13\VitrineBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \mi13\VitrineBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lignes_commande;


    /**
     * Add lignesCommande
     *
     * @param \mi13\VitrineBundle\Entity\LigneCommande $lignesCommande
     *
     * @return Commande
     */
    public function addLignesCommande(\mi13\VitrineBundle\Entity\LigneCommande $lignesCommande)
    {
        $this->lignes_commande[] = $lignesCommande;

        return $this;
    }

    /**
     * Remove lignesCommande
     *
     * @param \mi13\VitrineBundle\Entity\LigneCommande $lignesCommande
     */
    public function removeLignesCommande(\mi13\VitrineBundle\Entity\LigneCommande $lignesCommande)
    {
        $this->lignes_commande->removeElement($lignesCommande);
    }

    /**
     * Get lignesCommande
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignesCommande()
    {
        return $this->lignes_commande;
    }
}
