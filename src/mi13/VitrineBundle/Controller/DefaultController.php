<?php

namespace mi13\VitrineBundle\Controller;

use mi13\VitrineBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction($name) {
        $bestSellers = $this->get('produits_plus_vendus')->getPopularProducts(3);
        $articles = [];
        foreach ($bestSellers as $articleId => $quantitySold) {
            $articles[$quantitySold] = 
                    $this->getDoctrine()->getManager()
                    ->getRepository('mi13VitrineBundle:Article')
                    ->find($articleId);
        }
        return $this->render('mi13VitrineBundle:Default:index.html.twig',
                array('name' => $name, 'articles' => $articles));
    }
    
    public function signUpAction(Request $request) {
        $client = new Client();
        $form = $this->createForm('mi13\VitrineBundle\Form\ClientType', $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($client, $client->getPassword());
            $client->setPassword($encoded);
            $em->persist($client);
            $em->flush();

            return $this->redirectToRoute('mi13_vitrine_accueil', array('name' => $client->getName()));
        }

        return $this->render('mi13VitrineBundle:Default:signup.html.twig', array(
            'client' => $client,
            'form' => $form->createView(),
        ));
    }
    
    public function mentionsAction() {
        return $this->render('mi13VitrineBundle:Default:mentions.html.twig');
    }
    
    public function catalogueAction() {
        $categories = $this->getDoctrine()->getManager()->getRepository('mi13VitrineBundle:Categorie')
                ->findAll();
        return $this->render('mi13VitrineBundle:Default:catalogue.html.twig',
                array('categories' => $categories));
    }
    
    public function articlesParCategorieAction($id_categorie) {
        $manager = $this->getDoctrine()->getManager();
        $categorie = $manager->getRepository('mi13VitrineBundle:Categorie')->findOneBy(array('id' => $id_categorie));
        $articles = $manager->getRepository('mi13VitrineBundle:Article')
                ->findBy(array('categorie' => $id_categorie));
        return $this->render('mi13VitrineBundle:Default:articlesParCategorie.html.twig',
                array('categorie' => $categorie, 'articles' => $articles));
    }
    
    public function backofficeAction() {
        return $this->render('mi13VitrineBundle:Default:backoffice.html.twig');
    }
        
}
