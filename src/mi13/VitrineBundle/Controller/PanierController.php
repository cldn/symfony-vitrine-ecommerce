<?php

namespace mi13\VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PanierController extends Controller {

    public function contenuPanierAction(Request $request) {
        $panier = $request->getSession()->get('panier');
        checkPanier($panier);
        $total = $this->get('montant_panier')->getMontantPanier($panier);
        $articles = [];
        if ($panier->getContenu() != null) {
            foreach ($panier->getContenu() as $articleId => $quantite) {
                $articles[$articleId] = $this->getDoctrine()->getManager()
                        ->getRepository('mi13VitrineBundle:Article')
                        ->findOneBy(array('id' => $articleId));
            }
        }
        return $this->render('mi13VitrineBundle:Panier:contenuPanier.html.twig', array('panier' => $panier, 'articles' => $articles, 'total' => $total));
    }

    public function ajoutArticleAction(Request $request, $id_article, $quantite) {
        $session = $request->getSession();
        $panier = $session->get('panier');
        checkPanier($panier);
        $article = $this->getDoctrine()->getManager()
                ->getRepository('mi13VitrineBundle:Article')
                ->findOneBy(array('id' => $id_article));
        if($article->getStock() > 0) {
            $panier->ajoutArticle($id_article, $quantite != null ? $quantite : 1);
            $session->set('panier', $panier);
        } 
        return $this->contenuPanierAction($request);
    }

    public function viderPanierAction(Request $request) {
        $session = $request->getSession();
        $panier = $session->get('panier');
        checkPanier($panier);
        $panier->viderPanier();
        return $this->contenuPanierAction($request);
    }

    public function panierBlockAction(Request $request) {
        $panier = $request->getSession()->get('panier');
        $articles = [];
        checkPanier($panier);
        $total = $this->get('montant_panier')->getMontantPanier($panier);
        foreach ($panier->getContenu() as $articleId => $quantite) {
            $articles[$articleId] = $this->getDoctrine()->getManager()
                    ->getRepository('mi13VitrineBundle:Article')
                    ->findOneBy(array('id' => $articleId));
        }
        return $this->render('mi13VitrineBundle:Panier:panierBlock.html.twig', array('panier' => $panier, 'articles' => $articles, 'total' => $total));
    }
    
    public function validationPanierAction(Request $request) {
        $panier = $request->getSession()->get('panier');
        $commande = $this->get('panier_to_commande')->from($panier);
        $panier->viderPanier();
        return $this->render('mi13VitrineBundle:Panier:validationPanier.html.twig', array('commande' => $commande));
    }

}

function checkPanier(&$panier) {
    if ($panier == null) {
        $panier = new \mi13\VitrineBundle\Entity\Panier();
    }
}
