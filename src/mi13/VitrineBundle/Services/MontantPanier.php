<?php

namespace mi13\VitrineBundle\Services;

/**
 * Description of MontantPanier
 *
 * @author denailly
 */
class MontantPanier {
    
    private $entity_manager;
    
    public function __construct(\Doctrine\ORM\EntityManager $entity_manager) {
        $this->entity_manager = $entity_manager;
    }
    
    public function getMontantPanier(\mi13\VitrineBundle\Entity\Panier $panier) {
        $montant = 0;
        if ($panier != null) {
            foreach ($panier->getContenu() as $articleId => $quantite) {
                $article = $this->entity_manager->getRepository('mi13VitrineBundle:Article')
                        ->findOneBy(array('id' => $articleId));
                $montant += ($article->getPrice() * $quantite);
            }
        }
        return $montant;
    }
}
