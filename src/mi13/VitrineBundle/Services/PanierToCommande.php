<?php

namespace mi13\VitrineBundle\Services;

use mi13\VitrineBundle\Entity;

/**
 * Description of PanierToCommande
 *
 * @author denailly
 */
class PanierToCommande {

    private $entity_manager;

    public function __construct(\Doctrine\ORM\EntityManager $entity_manager) {
        $this->entity_manager = $entity_manager;
    }

    public function from(Entity\Panier $panier) {
        if ($panier != null && $panier->getContenu() != null) {
            $commande = new Entity\Commande();
            $commande->setDate(new \DateTime("now"));
            $commande->setValidated(false);
            $this->entity_manager->persist($commande);
            foreach ($panier->getContenu() as $articleId => $quantite) {
                $article = $this->entity_manager
                        ->getRepository('mi13VitrineBundle:Article')
                        ->findOneBy(array('id' => $articleId));
                $article->setStock($article->getStock() - $quantite);
                $this->addLigne($article, $quantite, $commande);
            }
            $this->entity_manager->flush();
            return $commande;
        }
    }

    public function addLigne($article, $quantite, $commande) {
        $ligne = new Entity\LigneCommande();
        $ligne->setArticle($article);
        $ligne->setPrix($article->getPrice());
        $ligne->setQuantite($quantite);
        $ligne->setCommande($commande);
        $this->entity_manager->persist($ligne);
        $this->entity_manager->flush();
    }

}
