<?php

namespace mi13\VitrineBundle\Services;

use mi13\VitrineBundle\Entity;

/**
 *
 * @author denailly
 */
class ProduitsPlusVendus {

    private $entity_manager;

    public function __construct(\Doctrine\ORM\EntityManager $entity_manager) {
        $this->entity_manager = $entity_manager;
    }
    
    // Count the most popular (best-sellers) articles and return the first $productCount results
    public function getPopularProducts($productCount) {
        $ventesArticles = [];
        // Get all the existing articles
        $articles = $this->entity_manager
                ->getRepository('mi13VitrineBundle:Article')
                ->findAll();
        // For each article in the repository
        foreach ($articles as $article) {
            // Prepare the "ventesArticles" array with a 0 at the article's ID index
            $ventesArticles[$article->getId()] = 0;
            // For each order line where the article appears
            foreach($article->getLignesCommande() as $ligne) {
                // Increase the matching array value with the quantity sold in this order line
                $ventesArticles[$article->getId()] += $ligne->getQuantite();
            }
        }
        // Revert-sort the result and keep the indexes
        arsort($ventesArticles);
        // Prepare a slice of the "ventesArticles" array, starting at the first index, for $productCount elements
        // The 4th parameters set to true allows the returned array to keep its indexes (which are the product IDs)
        $popularProducts = array_slice($ventesArticles, 0, $productCount, true);
        return $popularProducts;
    }

}
